FROM openjdk:8-jdk

ENV ANDROID_COMPILE_SDK "30"
ENV ANDROID_BUILD_TOOLS "29.0.3"

ENV ANDROID_HOME /android-sdk-linux
ENV PATH="${PATH}:/android-sdk-linux/platform-tools/"

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes git wget tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev
RUN apt-get --quiet install --yes vim-common
RUN mkdir -p ${ANDROID_HOME} && cd ${ANDROID_HOME} \
    && wget --quiet --output-document=cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip \
    && unzip cmdline-tools.zip && mv cmdline-tools latest && mkdir cmdline-tools && mv latest cmdline-tools/ && rm cmdline-tools.zip && cd -
RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "platform-tools" > /dev/null
RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null
RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null
RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager --licenses > /dev/null

COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundler
RUN bundle install
